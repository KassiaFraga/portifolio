��          �   %   �      @  l   A     �     �     �  w   �  9   O     �  S   �     �     �               1     7     ?     G     V     g     s  '     
   �     �     �     �  `  �  S   H     �     �     �  v   �  5   .     d  G   l     �     �     �     �     �     �     �     �  
     	     	      $   *     O     W     h     u                       	                                               
                                                         "interfaces web.", "aplicações mobile.", "automações e integrações de plataforma.", "experiências."  Automatização de tarefas Brasil Cidade Construção de painéis interativos e integrados a diversas aplicações usando Google DataStudio ou Microsoft PowerBI Construção de plataformas operacionais usando REST APIs Contato Criação de aplicações no Google Workspace para automatizar processos rotineiros Desenvolvedora FullStack Desenvolvimento WEB Eu construo Gerente de Produto Idade Inglês Início Meus serviços Paineis de dados Portifólio Residência Um pouco sobre o meu <br /> trabalho... Visualizar anos de experiência certificados projetos completos Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-01-05 09:36-0300
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 "web interfaces.", "mobile applications.", "platforms integrations", "experiencies" Automate tasks Brazil City Construction of interactive panels and integrated to several applications using Google DataStudio or Microsoft PowerBI Construction of operational platforms using REST APIs Contact Creating applications in Google Workspace to automate routine processes FullStack Developer WEB Development I build Product Owner Age English Home My services Dashboards Portfolio Residence A little bit about <br /> my work... Explore years experience certificates completed projects 