$(document).ready(function() 
{
   $.ajaxSetup({
      beforeSend: function(xhr) {
          function getCookie(name) {
              var cookieValue = null;
              if (document.cookie && document.cookie !== '') {
                  var cookies = document.cookie.split(';');
                  for (var i = 0; i < cookies.length; i++) {
                      var cookie = cookies[i].trim();
                      // Does this cookie string begin with the name we want?
                      if (cookie.substring(0, name.length + 1) === (name + '=')) {
                          cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                          break;
                      }
                  }
              }
              return cookieValue;
          }

          var csrftoken = getCookie('csrftoken');

          xhr.setRequestHeader("X-CSRFToken", csrftoken);
      }
  });

   $('ul.language-switcher li').click(function(e) 
   { 
      var language = $(this).data("value");
      $.ajax({
         type: "POST",
         url: `/change_language/`,
         data: { 'language': language }
      }).done(function() {
         window.location.href = language == 'pt-br' ? `/` : `/${language}/`;
      });
   });
});