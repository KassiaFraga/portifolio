from django.contrib import admin
from django.urls import include, path
from django.conf.urls.i18n import i18n_patterns 
from django.utils.translation import gettext_lazy as _

from . import views

app_name = "base"
urlpatterns = [
    path("", views.index, name="index"),
    path('change_language/', views.change_language, name='change_language')
]